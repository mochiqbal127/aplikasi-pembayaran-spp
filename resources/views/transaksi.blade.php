@extends('tmplt/header')

@push('style')
    <style>

    </style>
@endpush
@section('content')
@include('tmplt.feedback')
<section class="content-header">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Transaksi</h1>
    </div>
</section>
<section class="content">
    <form action="{{ url('transaksi')}}" method="POST">
        {{ csrf_field() }}
    <div class="card shadow mb-4" id="card-data-siswa">
        <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
            <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Siswa</h6>
            <a href="#add" data-toggle="modal" data-target="#ModalDataSiswa" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Pilih</a>
        </div>
        <input id="sppDipilih" type="hidden" name="id_spp" class="form-control">
        <input value="{{Auth::user()->id_petugas}}" type="hidden" name="id_petugas" class="form-control" readonly>
        <div class="card-body text-gray-800">
            <div class="form-row mb-3">
                <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">NISN</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="nisnDipilih" type="text" name="nisn" class="form-control" readonly onchange="hpsrow()">
                </div>
            </div>
            <div class="form-row mb-3">
                <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">Nama</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="namaDipilih" type="text" name="nama" class="form-control" readonly>
                </div>
            </div>
            <div class="form-row mb-3">
                <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">Kelas</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="kelasDipilih" type="text" name="kelas" class="form-control" readonly>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4" id="card-data-spp">
        <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
            <h6 class="h5 m-0 font-weight-bold text-gray-100">SPP</h6>
            <a href="#ads" data-toggle="modal" data-target="#ModalSpp" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="card-body">
            <table id="tblbayar" class="table table-striped text-gray-800" width="100%">
                <thead>
                    <tr>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Nominal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" "><center>Jumlah Total</center> </th>
                        <th colspan="2" ">Rp. <span id='totalBayar'>0</span></th>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="card shadow mb-4" id="card-bayar">
        <div class="card-body text-gray-800">
            <div class="form-row mb-3">
                <label class="col col-md-3 col-sm-3 col-xs-12">Jumlah Bayar</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="number" name="bayar" id="bayar" class="form-control">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                <button href="#" id="btnbayar" class=" btn btn-primary" type="submit">
                    Bayar
                </button>
            </div>
            </div>
        </div>
    </div>
    <div class="modal" id="okModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header bg-primary text-gray-100">
                <h5 class="modal-title" id="exampleModalLabel">Kembalian</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_hapus" id="idHapus">
                Total Kembalian Rp. <b id="datakembali"></b>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Ok</button>
                {{-- <a href="" type="submit" id="btnHapus" class="btn btn-danger">Delete</a> --}}
            </div>
        </form>
            </div>
        </div>
    </div>
</form>
</section>
@endsection
@include('transaksi/form')
@push('script')
    <script>
        $(function(){
            $('#okModal').modal({backdrop : 'static',keyboard : false});
            $('#okModal').modal('hide');
            $('#dtSiswa').DataTable();
            $('#dtSpp').DataTable();


            $('#dtSiswa').on("click",".add-siswa",function(){
                let barisDipilih = $(this).closest("tr");
                let nama = barisDipilih.find("td:eq(2)").text();
                let nisn = barisDipilih.find("td:eq(1)").text();
                let kelas = barisDipilih.find("td:eq(3)").text();
                let ids = barisDipilih.find("#sppnih").val();
                let tahun = barisDipilih.find("#tahunnih").val();
                let nominal = barisDipilih.find("#nominalnih").val();


                $('#namaDipilih').val(nama);
                $('#nisnDipilih').val(nisn);
                $('#kelasDipilih').val(kelas);
                $('#sppDipilih').val(ids);
                $('#dtSpp .tahun').text(tahun);
                $('#dtSpp .nominal').text(nominal);


                $('#ModalDataSiswa').modal('hide');

                $('#tblbayar tbody tr').remove();
            });


            $('#dtSpp').on("click",".add-spp",function(){
                let barisDipilih;
                barisDipilih = $(this).closest("tr");
                let tahun = barisDipilih.find("td:eq(2)").text();
                let bulan = barisDipilih.find("td:eq(1)").text();
                let nominal = barisDipilih.find("td:eq(3)").text();
                let act = "<input type='button' value='x' class='btn btn-sm btn-danger conf-modal' data-toggle='modal' data-target='#confModal'>";

                let addRow = "<tr><td><input type='text' name='bulan_dibayar[]' value='"+bulan+"' readonly class='form-control bulan'></td></td>";
                addRow += "<td><input type='text' name='tahun_dibayar[]' value='"+tahun+"' readonly class='form-control tahun'></td>";
                addRow += "<td><input type='text' name='jumlah_bayar[]' value='"+nominal+"' readonly class='form-control nominal'></td>";
                addRow += "<td>"+act+"</td></tr>";

                let totalBayar = parseFloat($('#totalBayar').text());
                let harga = parseFloat(nominal);
                totalBayar += harga;
                $('#totalBayar').text(totalBayar);
                $('#tblbayar tbody').append(addRow);
                barisDipilih.remove();
                $('#ModalSpp').modal('hide');

            });

            $('#card-data-spp').on("click",".add-spp",function(){
                let bulan = "<input type='text' class='form-control bulan'>";
                let tahun = "<input type='text' class='form-control tahun'>";
                let nominal = "<input type='number' class='form-control nominal'>";
                let jumlah = "<input type='number' class='form-control bayar'>";
                let act = "<input type='button' value='x' class='btn btn-sm btn-danger conf-modal' data-toggle='modal' data-target='#confModal'>";


                let addRow = "<tr><td>"+bulan+"</td>";
                addRow += "<td>"+tahun+"</td>";
                addRow += "<td>"+nominal+"</td>";

                addRow += "<td>"+act+"</td></tr>";

                $('#tblbayar tbody').append(addRow);

            });

           $('#card-data-spp').on("click",".conf-modal",function(){
                let rowSlc = $(this).closest('tr');
                let tahun = rowSlc.find(".tahun").val();
                let bulan = rowSlc.find(".bulan").val();
                let no = rowSlc.find(".nominal").val();

                $('#confModal').on("click",".delete-row",function(){
                    rowSlc.remove();
                    let kurang = parseFloat($('#totalBayar').text());
                    let hargaa = parseFloat($('#tblbayar .nominal').val());

                    let baris = "<tr><td>#</td>";
                    baris += "<td>"+bulan+"</td>";
                    baris += "<td>"+tahun+"</td>";
                    baris += "<td>"+no+"</td>";
                    baris += "<td><button class='add-spp btn btn-primary'>Add</button></td></tr>";
                    $('#dtSpp tr:first').after(baris);

                    kurang -= hargaa;
                    $('#totalBayar').text(kurang);
                    $('#confModal').modal('hide');
                });
           });

        })
    </script>
@endpush

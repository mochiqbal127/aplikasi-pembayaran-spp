
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary text-gray-100">
            <h5 class="modal-title" id="myModalLabel">Tambah Data SPP</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('spp')}}" method="POST">
                {{ csrf_field() }}
                <div id="method"></div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun <span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputTahun" name="tahun" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nominal<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input  type="text" id="inputNominal" name="nominal" required="required" class="form-control ">
                    </div>
                </div>
                <div class="ln_solid"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger text-gray-100">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data SPP</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="id_hapus" id="idHapus">
            Apakah data <b id="dataHapus"></b> ini akan dihapus?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <a href="" type="submit" id="btnHapus" class="btn btn-danger">Delete</a> --}}
            <div id="btnHapus"></div>
        </div>
    </form>
        </div>
</div>
</div>

{{-- @extends('tmplt/header')

@section('content')
<div class="row">
    @include('tmplt/feedback')
    <div class="col-md-12 col-sm-12 col-xs-12 card shadow">
        <div class="py-3">
        <div class="card-header bg-primary">
            <h2 class="h5 mt-3 text-gray-100">{{ empty($spp) ? 'Tambah' : 'Edit' }} Data SPP</h2>
        </div>
        <div class="card-body">

        <form class="text-gray-800" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ empty($spp) ? url('spp/add') : url("spp/$spp->id_spp/edit")}}" method="POST">
            {{ csrf_field() }}

            @if(!empty($spp))
                {{ method_field('PUT') }}
            @endif
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tahun<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$spp->tahun}}" type="text" id="tahun" name="tahun" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nominal<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$spp->nominal}}" type="text" id="nominal" name="nominal" required="required" class="form-control ">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button"><a href="{{route('kelas')}}" style="color: white">Cancel</a></button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>

            </form>
        </div>
        </div>
    </div>
</div>
@endsection --}}

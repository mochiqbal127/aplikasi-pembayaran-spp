<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary text-gray-100">
            <h5 class="modal-title" id="myModalLabel">Tambah Data </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('petugas')}}" method="POST">
                {{ csrf_field() }}
                <div id="method"></div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Username <span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputUser" name="username" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Password<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input  type="password" id="inputPass" name="password" required="required" class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nama Petugas<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNama" name="nama_petugas" required="required" class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Level<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    {{-- <input type="text" id="inputLevel" name="level" required="required" class="form-control "> --}}
                    <select name="level" id="inputLevel" class="form-control">
                        <option value="admin">Admin</option>
                        <option value="petugas">Petugas</option>
                    </select>
                    </div>
                </div>
                <div class="ln_solid"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger text-gray-100">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Petugas</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="id_hapus" id="idHapus">
            Apakah data <b id="dataHapus"></b> ini akan dihapus?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <a href="" type="submit" id="btnHapus" class="btn btn-danger">Delete</a> --}}
            <div id="btnHapus"></div>
        </div>
    </form>
        </div>
    </div>
</div>

{{-- @extends('tmplt/header')

@section('content')
<div class="row">
    @include('tmplt/feedback')
    <div class="col-md-12 col-sm-12 col-xs-12 card shadow">
        <div class="py-3">
        <div class="card-header bg-primary">
            <h2 class="h5 mt-3 text-gray-100">{{ empty($petugas) ? 'Tambah' : 'Edit' }} Data Petugas</h2>
        </div>
        <div class="card-body">

        <form class="text-gray-800" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ empty($petugas) ? url('petugas/add') : url("petugas/$petugas->id_petugas/edit")}}" method="POST">
            {{ csrf_field() }}

            @if(!empty($petugas))
                {{ method_field('PUT') }}
            @endif
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$petugas->username}}" type="text" id="username" name="username" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="text" id="password" name="password" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Petugas<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$petugas->nama_petugas}}" type="text" id="nama_petugas" name="nama_petugas" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Level<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$petugas->level}}" type="text" id="level" name="level" required="required" class="form-control ">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button"><a href="{{route('petugas')}}" style="color: white">Cancel</a></button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>

            </form>
        </div>
        </div>
    </div>
</div>
@endsection --}}

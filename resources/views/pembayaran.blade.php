@extends('tmplt/header')

@section('content')
@include('tmplt.feedback')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pembayaran</h1>
</div>
<div class="card shadow mb-4" id="cardata">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Siswa</h6>
        <a href="" data-toggle="modal" data-target="#ModalDataSiswa" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Pilih</a>
    </div>
    <div class="card-body text-gray-800">
        <div class="form-row mb-3">
            <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">NISN</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="inputNisn" type="text" name="nisn" required="required" class="form-control">
            </div>
        </div>
        <div class="form-row mb-3">
            <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">Nama</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="inputNama" type="text" name="nama" required="required" class="form-control">
            </div>
        </div>
        <div class="form-row mb-3">
            <label class="col col-md-3 col-sm-3 col-xs-12" for="first-name">Kelas</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="inputKelas" type="text" name="Kelas" required="required" class="form-control">
            </div>
        </div>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="ModalDataSiswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pilih Data Siswa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-striped" id="dtSiswa" width="100%">
                <thead id="tblHead">
                  <tr>
                    <th>No</th>
                    <th>NISN</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($siswa as $anak)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                        <td>{{ $anak->nis }}</td>
                        <td>{{ $anak->nama }}</td>
                        <td>{{ $anak->kelas->nama_kelas}}</td>
                        <td><center>
                            <a id="plSiswa" href="#add" data-dismiss="modal" data-target="#cardata" data-nisn="{{$anak->nisn}}" data-nis="{{$anak->nis}}" data-nama="{{$anak->nama}}" data-idkel="{{$anak->id_kelas}}" data-alamat="{{$anak->alamat}}" data-notelp="{{$anak->no_telp}}" data-idspp="{{$anak->id_spp}}" class="btn btn-sm btn-primary">Pilih</a>
                            {{-- <a href="#edit" data-toggle="modal" data-target="#confModal" data-nisn="{{$anak->nisn}}" data-nama="{{$anak->nama}}" class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a> --}}
                        </center></td>
                    </tr>
                    @endforeach
              </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          {{-- <button type="button" class="btn btn-primary">Pilih</button> --}}
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script>
        $(function(){
            $('#dtSiswa').DataTable();

        })
    </script>
@endpush

@extends('tmplt/header')

@section('content')
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data SPP</h6>
        <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtspp" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Tahun SPP</th>
                <th>Nominal</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($colec as $row)
            <tr>
                <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                <td>{{ $row->tahun }}</td>
                <td>{{ $row->nominal }}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit" data-id="{{$row->id_spp}}" data-tahun="{{$row->tahun}}" data-nominal="{{$row->nominal}}" class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                    <a href="#edit" data-toggle="modal" data-target="#confModal" data-id="{{$row->id_spp}}" data-tahun="{{$row->tahun}}" class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('spp/form')
@push('script')
<script>
    $(function(){
        $('#dtspp').DataTable();
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var tahun = button.data('tahun');
            var nominal = button.data('nominal');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data SPP');
                modal.find('.modal-body #inputTahun').val(tahun);
                modal.find('.modal-body #inputNominal').val(nominal);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" name="id_spp" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data SPP');
                modal.find('.modal-body #inputTahun').val('');
                modal.find('.modal-body #inputNominal').val('');
                modal.find('.modal-body #method').html('');

            }
        });
        $('#confModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var tahun = button.data('tahun');
            var modal = $(this);
            modal.find('.modal-footer #btnHapus').html('<a href="spp/'+id+'/hapus" type="submit" id="btnHapus" class="btn btn-danger">Delete</a>');
            modal.find('.modal-body #dataHapus').text(tahun);
            });
    })
</script>

@endpush

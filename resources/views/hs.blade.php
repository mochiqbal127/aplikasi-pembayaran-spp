@extends('tmplt/headersiswa')

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Riwayat Pembayaran</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dthistori" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Tanggal Bayar</th>
                <th>Bulan diBayar</th>
                <th>Tahun diBayar</th>
                <th>Jumlah Bayar</th>
                {{-- <th>Action</th> --}}
            </tr>
            </thead>
            <tbody>
            @foreach ($histori as $row)
            <tr>
                <th scope="row">{{ !empty($i) ? ++$i : $i = 1 }}</th>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->nama_kelas }}</td>
                <td>{{ $row->tgl_bayar }}</td>
                <td>{{ $row->bulan_dibayar }}</td>
                <td>{{ $row->tahun_dibayar }}</td>
                <td>{{ $row->jumlah_bayar }}</td>
                {{-- <td>
                    <button class="btn btn-primary">Detail</button>
                </td> --}}
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
@push('script')
    <script>
        $('#dthistori').DataTable();
    </script>
@endpush

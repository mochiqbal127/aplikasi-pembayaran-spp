@extends('tmplt/header')

@section('content')
<!-- DataTales Example -->
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Kelas</h6>
        <span class="d-sm-inline-block">

            <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add"  class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
            <a href="{{route('cetak-kelas')}}" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-download"></i> Cetak</a> 
        </span>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtkelas" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Nama Kelas</th>
                <th>Kompetensi Keahlian</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($colec as $row)
            <tr>
                <th scope="row">{{ !empty($i) ? ++$i : $i = 1 }}</th>
                <td>{{ $row->nama_kelas }}</td>
                <td>{{ $row->kompetensi_keahlian }}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit" data-id="{{$row->id_kelas}}" data-nama="{{$row->nama_kelas}}" data-komp="{{$row->kompetensi_keahlian}}" class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>

                    <a href="#edit" data-toggle="modal" data-target="#confModal" data-nama="{{$row->nama_kelas}}" data-id="{{$row->id_kelas}}" class="btn btn-sm btn-danger" ><i class='fa fa-trash'></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
@include('kelas/form')
@push('script')
<script>
    $(function(){
        $('#dtkelas').DataTable();
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var nama = button.data('nama');
            var komp = button.data('komp');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Kelas');
                modal.find('.modal-body #inputNama').val(nama);
                modal.find('.modal-body #inputKomp').val(komp);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" name="id_kelas" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Kelas');
                modal.find('.modal-body #inputNama').val('');
                modal.find('.modal-body #inputKomp').val('');
                modal.find('.modal-body #method').html('');

            }
        });
    })

    $(function(){
        $('#confModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var nama = button.data('nama');
            var modal = $(this);
            modal.find('.modal-footer #btnHapus').html('<a href="kelas/'+id+'/hapus" type="submit" id="btnHapus" class="btn btn-danger">Delete</a>');
            modal.find('.modal-body #dataHapus').text(nama);
            });
    })
</script>

@endpush

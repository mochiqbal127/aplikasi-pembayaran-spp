<html lang="en">
<head>

    <title>Cetak</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{asset('assets')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
    <link href="{{asset('assets')}}/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<body>
	<center>
		<h5>Membuat Laporan PDF Dengan DOMPDF Laravel</h5>
	</center>
    <div class="">
        <div class="">
            <div class="">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Kelas</th>
                    <th>Kompetensi Keahlian</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($kelas as $row)
                <tr>
                    <th scope="row">{{ !empty($i) ? ++$i : $i = 1 }}</th>
                    <td>{{ $row->nama_kelas }}</td>
                    <td>{{ $row->kompetensi_keahlian }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</body>
</html>



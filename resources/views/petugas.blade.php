@extends('tmplt/header')

@section('content')
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Petugas</h6>
        <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtpetugas" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Username</th>
                <th>Nama Petugas</th>
                <th>Level</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($petugas as $org)
            <tr>
                <th scope="row">{{ !empty($i) ? ++$i : $i = 1 }}</th>
                <td>{{ $org->username }}</td>
                <td>{{ $org->nama_petugas }}</td>
                <td>{{ $org->level_display }}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit" data-id="{{$org->id_petugas}}"
                        data-user="{{$org->username}}" data-pass="{{$org->password}}" data-level="{{$org->level}}"
                        data-nama="{{$org->nama_petugas}}" class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                    <a href="#edit"  data-toggle="modal" data-target="#confModal" data-mode="edit" data-id="{{$org->id_petugas}}" data-nama="{{$org->nama_petugas}}"  class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('petugas/form')
@push('script')
<script>
    $(function(){
        $('#dtpetugas').DataTable();
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var user = button.data('user');
            var pass = button.data('pass');
            var lvl = button.data('level');
            var nama = button.data('nama');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Petugas');
                modal.find('.modal-body #inputNama').val(nama);
                modal.find('.modal-body #inputUser').val(user);
                modal.find('.modal-body #inputLevel').val(lvl);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" name="id_petugas" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Petugas');
                modal.find('.modal-body #inputNama').val('');
                modal.find('.modal-body #inputUser').val('');
                modal.find('.modal-body #inputLevel').val('');
                modal.find('.modal-body #method').html('');

            }
        });
    })

    $(function(){
        $('#confModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var tahun = button.data('nama');
            var modal = $(this);
            modal.find('.modal-footer #btnHapus').html('<a href="petugas/'+id+'/hapus" type="submit" id="btnHapus" class="btn btn-danger">Delete</a>');
            modal.find('.modal-body #dataHapus').text(tahun);
            });
    })
</script>

@endpush

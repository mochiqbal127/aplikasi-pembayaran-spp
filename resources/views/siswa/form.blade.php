<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary text-gray-100">
            <h5 class="modal-title" id="myModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('siswa')}}" method="POST">
                {{ csrf_field() }}
                <div id="method"></div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NISN<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNisn" name="nisn" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NIS<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNis" name="nis" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNama" name="nama" required="required" class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Kelas<span class="required"> *</span></label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <select class="form-control" name="id_kelas" id="inputIdKelas" >
                        @foreach (\App\Kelas::all() as $kelas)
                        <option value="{{ $kelas->id_kelas }}">
                        {{ $kelas->nama_kelas}}
                        </option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputAlamat" name="alamat" required="required" class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Telepon<span class="required"> *</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNoTelp" name="no_telp" required="required" class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Tahun SPP<span class="required"> *</span></label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <select class="form-control" name="id_spp" id="inputIdSpp" >
                        @foreach (\App\SPP::all() as $spp)
                        <option value="{{ $spp->id_spp }}">
                        {{ $spp->tahun}}
                        </option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="ln_solid"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger text-gray-100">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Petugas</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="id_hapus" id="idHapus">
            Apakah data <b id="dataHapus"></b> ini akan dihapus?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <a href="" type="submit" id="btnHapus" class="btn btn-danger">Delete</a> --}}
            <div id="btnHapus"></div>
        </div>
    </form>
        </div>
    </div>
</div>
{{-- @extends('tmplt/header')

@section('content')
<div class="row">
    @include('tmplt/feedback')
    <div class="col-md-12 col-sm-12 col-xs-12 card shadow">
        <div class="py-3">
        <div class="card-header bg-primary">
            <h2 class="h5 mt-3 text-gray-100">{{ empty($siswa) ? 'Tambah' : 'Edit' }} Data Siswa</h2>
        </div>
        <div class="card-body">

        <form class="text-gray-900" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ empty($siswa) ? url('siswa/add') : url("siswa/$siswa->nisn/edit")}}" method="POST">
            {{ csrf_field() }}

            @if(!empty($siswa))
                {{ method_field('PUT') }}
            @endif
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NISN<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$siswa->nisn}}" type="text" id="nisn" name="nisn" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NIS<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$siswa->nis}}" type="text" id="nis" name="nis" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$siswa->nama}}" type="text" id="nama" name="nama" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Kelas<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="id_kelas" id="id_kelas" >
                    @foreach (\App\Kelas::all() as $kelas)
                    <option value="{{ $kelas->id_kelas }}" {{ @$siswa->id_kelas == $kelas->id_kelas ? 'selected' : '' }}>
                    {{ $kelas->nama_kelas}}
                    </option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$siswa->alamat}}" type="text" id="alamat" name="alamat" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Telepon<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{@$siswa->no_telp}}" type="text" id="no_telp" name="no_telp" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Tahun SPP<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="id_spp" id="id_spp" >
                    @foreach (\App\SPP::all() as $spp)
                    <option value="{{ $spp->id_spp }}" {{ @$siswa->id_spp == $spp->id_spp ? "selected" : "" }}>
                    {{ $spp->tahun}}
                    </option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button"><a href="{{route('petugas')}}" style="color: white">Cancel</a></button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>

            </form>
        </div>
        </div>
    </div>
</div>
@endsection --}}

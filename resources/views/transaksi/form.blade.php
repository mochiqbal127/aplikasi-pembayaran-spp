<!-- Modal Siswa-->
<div class="modal fade" id="ModalDataSiswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Pilih Data Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="dtSiswa" width="100%">
                    <thead id="tblHead">
                    <tr>
                        <th>No</th>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Kelas</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($siswatr as $anak)
                        <tr>
                            <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                            <td>{{ $anak->nisn }}</td>
                            <td>{{ $anak->nama }}</td>
                            <td>{{ $anak->nama_kelas}}</td>
                            <td>
                                <input id="sppnih" type="hidden" name="id_spp" value="{{ $anak->id_spp}}">
                                <input id="tahunnih" type="hidden" name="id_spp" value="{{ $anak->tahun}}">
                                <input id="nominalnih" type="hidden" name="id_spp" value="{{ $anak->nominal}}">
                                <button class="add-siswa btn btn-primary">Add</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary">Pilih</button> --}}
            </div>
        </div>
    </div>
</div>
<!-- end Modal Siswa-->
<!-- Modal SPP-->
<div class="modal fade" id="ModalSpp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Pilih Bulan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="dtSpp" width="100%">
                    <thead id="tblHead">
                    <tr>
                        <th>No</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Nominal</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</th>
                            <td>Juli</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</th>
                            <td>Agustus</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</th>
                            <td>September</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</th>
                            <td>Oktober</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>5</th>
                            <td>November</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>6</th>
                            <td>Desember</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>7</th>
                            <td>Januari</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>8</th>
                            <td>Februari</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>9</th>
                            <td>Maret</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>10</th>
                            <td>April</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>11</th>
                            <td>Mei</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                        <tr>
                            <td>12</th>
                            <td>Juni</td>
                            <td><span class="tahun"></span></td>
                            <td><span class="nominal"></span></td>
                            <td>
                                <button class="add-spp btn btn-primary">Add</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary">Pilih</button> --}}
            </div>
        </div>
    </div>
</div>
<!-- end Modal SPP-->
<div class="modal fade" id="confModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-gray-100">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Baris</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_hapus" id="idHapus">
                Apakah baris ini akan dihapus?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-row" >Delete</button>
            </div>
        </div>
    </div>
</div>

@extends('tmplt/header')

@section('content')
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Siswa</h6>
        <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtsiswa" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>NISN</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Nomor Telepon</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($siswa as $anak)
            <tr>
                <th scope="row">{{ !empty($i) ? ++$i : $i = 1 }}</th>
                <td>{{ $anak->nisn }}</td>
                <td>{{ $anak->nama }}</td>
                <td>{{ $anak->kelas->nama_kelas}}</td>
                <td>{{ $anak->no_telp }}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit" data-id="{{$anak->id_siswa}}" data-nisn="{{$anak->nisn}}" data-nis="{{$anak->nis}}" data-nama="{{$anak->nama}}" data-idkel="{{$anak->id_kelas}}" data-alamat="{{$anak->alamat}}" data-notelp="{{$anak->no_telp}}" data-idspp="{{$anak->id_spp}}" class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                    <a href="#edit" data-toggle="modal" data-target="#confModal" data-nisn="{{$anak->id_siswa}}" data-nama="{{$anak->nama}}" class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('siswa/form')
@push('script')
<script>
    $(function(){
        $('#dtsiswa').DataTable();
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var nisn = button.data('nisn');
            var id = button.data('id');
            var mode = button.data('mode');
            var nis = button.data('nis');
            var nama = button.data('nama');
            var idkel = button.data('idkel');
            var alamat = button.data('alamat');
            var notelp = button.data('notelp');
            var idspp = button.data('idspp');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Siswa');
                modal.find('.modal-body #inputNisn').val(nisn);
                modal.find('.modal-body #inputNis').val(nis);
                modal.find('.modal-body #inputNama').val(nama);
                modal.find('.modal-body #inputIdKelas').val(idkel);
                modal.find('.modal-body #inputAlamat').val(alamat);
                modal.find('.modal-body #inputNoTelp').val(notelp);
                modal.find('.modal-body #inputIdSpp').val(idspp);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" name="id" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Siswa');
                modal.find('.modal-body #inputNisn').val('');
                modal.find('.modal-body #inputNis').val('');
                modal.find('.modal-body #inputNama').val('');
                modal.find('.modal-body #inputIdKelas').val('');
                modal.find('.modal-body #inputAlamat').val('');
                modal.find('.modal-body #inputNoTelp').val('');
                modal.find('.modal-body #inputIdSpp').val('');
                modal.find('.modal-body #method').html('');

            }
        });
    })

    $(function(){
        $('#confModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var nisn = button.data('nisn');
            var nama = button.data('nama');
            var modal = $(this);
            modal.find('.modal-footer #btnHapus').html('<a href="siswa/'+nisn+'/hapus" type="submit" id="btnHapus" class="btn btn-danger">Delete</a>');
            modal.find('.modal-body #dataHapus').text(nama);
            });
    })
</script>

@endpush

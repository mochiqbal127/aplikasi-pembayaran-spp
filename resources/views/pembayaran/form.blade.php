
@extends('tmplt/header')

@section('content')
<div class="row">
    @include('tmplt/feedback')
    <div class="col-md-12 col-sm-12 col-xs-12 card shadow">
        <div class="py-3">
        <div class="card-header bg-primary"><center>
            <h2 class="h3 mt-3 text-gray-100">{{ empty($pembayaran) ? 'Tambah' : 'Edit' }} Pembayaran</h2>
        </center></div>
        <div class="card-body">

        <form class="text-gray-900" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ empty($pembayaran) ? url('pembayaran/add') : url("pembayaran/$pembayaran->nisn/edit")}}" method="POST">
            {{ csrf_field() }}

            @if(!empty($siswa))
                {{ method_field('PUT') }}
            @endif
            <div class="form-group">
                <input type="hidden" value="{{Auth::user()->id_petugas}}" name="id_petugas">
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Siswa<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="nisn" id="nisn" >
                    @foreach (\App\Siswa::all() as $siswa)
                    <option value="{{ $siswa->nisn }}" {{ @$pembayaran->nisn == $siswa->nisn ? 'selected' : '' }}>
                    {{ $siswa->nama}}
                    </option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="date" id="" name="tgl_bayar" required="required" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bulan<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="text" id="bulan_dibayar" name="bulan_dibayar" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="text" id="tahun_dibayar" name="tahun_dibayar" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">ID SPP<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="text" id="id_spp" name="id_spp" required="required" class="form-control ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jumlah Bayar<span class="required"> *</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="" type="text" id="jumlah_bayar" name="jumlah_bayar" required="required" class="form-control ">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button"><a href="{{route('petugas')}}" style="color: white">Cancel</a></button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>

            </form>
        </div>
        </div>
    </div>
</div>
@endsection

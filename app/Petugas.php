<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    public $primaryKey = 'id_petugas';
    protected $table = 'petugas';
    protected $fillable = ['username','password','nama_petugas','level'];

    public function getLevelDisplayAttribute(){
        if(@$this->attributes['level']=='admin') return 'Admin';
        if(@$this->attributes['level']=='petugas') return 'Petugas';
        return '-';
    }
}

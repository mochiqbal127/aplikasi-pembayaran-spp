<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    public $primaryKey = 'id_siswa';
    protected $table = 'siswa';
    protected $fillable = ['nisn','nis','nama','id_kelas','alamat','no_telp','id_spp'];

    public function kelas(){
        return $this->hasOne('\App\Kelas', 'id_kelas','id_kelas');
    }
}

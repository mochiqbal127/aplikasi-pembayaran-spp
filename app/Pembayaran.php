<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    public $primaryKey = 'id_pembayaran';
    protected $table = 'pembayaran';
    protected $fillable = ['id_petugas','nisn','tgl_bayar','bulan_dibayar','tahun_dibayar','id_spp','jumlah_bayar'];

    public function siswa(){
        return $this->hasOne('\App\Siswa', 'nisn','nisn');
    }
}

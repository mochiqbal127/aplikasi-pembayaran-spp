<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SPP extends Model
{
    public $primaryKey = 'id_spp';
    protected $table = 'spp';
    protected $fillable = ['tahun','nominal'];
}

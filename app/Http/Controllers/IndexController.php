<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index(){
        if(Auth::user()->level == 'admin')
        {
            return view('index');
        }
        elseif(Auth::user()->level == 'siswa')
        {
            return view('indexsiswa');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use PDF;

class KelasController extends Controller
{
    public function index()
    {
        $data['colec'] = \App\Kelas::all();
        return view ('kelas')->with($data);
    }
    public function create(){
        return view('kelas/form');
    }
    public function store(Request $request){
        $rules=[
            'nama_kelas' => 'required',
            'kompetensi_keahlian' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = \App\Kelas::create($input);

        if($status) return redirect('/kelas')->with('success','Data Berhasil Disimpan!');
        else return redirect('/kelas')->with('error','Data gagal Disimpan!!');
    }
    public function edit($id){
        $data = Kelas::find($id);
        return view('kelas/form',['kelas'=>$data]);
    }
    public function update(Request $request){
        $rules=[
            'nama_kelas' => 'required',
            'kompetensi_keahlian' => 'required'
        ];
        $this->validate($request, $rules);

        $data = Kelas::find($request->id_kelas);
        $data->nama_kelas = $request->nama_kelas;
        $data->kompetensi_keahlian = $request->kompetensi_keahlian;
        $data->save();

        return redirect('/kelas')->with('success','Data Berhasil Diubah!');
    }

    public function delete($id){
        $genre = Kelas::find($id);
        $genre->delete();
        return redirect('/kelas')->with('success','Data Berhasil DiHapus');
    }

    public function cetak(){
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $kelas = Kelas::all();

        $pdf = PDF::loadview('kelas/cetak',['kelas'=>$kelas]);
        return $pdf->stream('cetak-kelas.pdf');
    }
}

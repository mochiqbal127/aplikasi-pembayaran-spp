<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Petugas;

class PetugasController extends Controller
{
    public function index()
    {
        $data['petugas'] = \App\Petugas::where('level','!=','siswa')->get();
        return view ('petugas')->with($data);
    }
    public function create(){
        return view('petugas/form');
    }
    public function store(Request $request){
        $rules=[
            'username' => 'required',
            'password' => 'required',
            'nama_petugas' => 'required',
            'level' => 'required'
        ];
        $this->validate($request, $rules);

        // $input = $request->all();
        // $status = \App\Petugas::create($input);
        $input = new Petugas;
        $input->username = $request->username;
        $input->password = bcrypt($request->password);
        $input->nama_petugas = $request->nama_petugas;
        $input->level = $request->level;
        $status = $input->save();


        if($status) return redirect('/petugas')->with('success','Data Berhasil Disimpan!');
        else return redirect('/petugas')->with('error','Data gagal Disimpan!!');
    }
    public function edit($id){
        $data = Petugas::find($id);
        return view('petugas/form',['petugas'=>$data]);
    }
    public function update(Request $request){
        $rules=[
            'username' => 'required',
            'password' => 'required',
            'nama_petugas' => 'required',
            'level' => 'required'
        ];
        $this->validate($request, $rules);

        // $input = $request->all();
        // $result = \App\Petugas::where('id_petugas',$id)->first();
        // $status = $result->update($input);
        $input = Petugas::find($request->id_petugas);
        $input->username = $request->username;
        $input->password = bcrypt($request->password);
        $input->nama_petugas = $request->nama_petugas;
        $input->level = $request->level;
        $status = $input->save();

        return redirect('/petugas')->with('success','Data Berhasil Diubah!');
    }

    public function delete($id){
        $petugas = Petugas::find($id);
        $petugas->delete();
        return redirect('/petugas')->with('success','Data Berhasil DiHapus');
    }
}

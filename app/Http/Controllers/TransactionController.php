<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transaksi;

class TransactionController extends Controller
{
    public function index(){
        $data['siswatr'] = DB::table('siswa')->join('kelas','siswa.id_kelas','=','kelas.id_kelas')->join('spp','siswa.id_spp','=','spp.id_spp')->get();
        // $data['siswatr'] = \App\Siswa::get();
        return view('transaksi')->with($data);
    }
    public function store(Request $request){

        for($i=0 ; $i < count($request['bulan_dibayar']);$i++){
            $input = new Transaksi();
            $input->id_petugas = $request->id_petugas;
            $input->nisn = $request->nisn;
            $input->tgl_bayar = date('Y-m-d');
            $input->bulan_dibayar = $request->bulan_dibayar[$i];
            $input->tahun_dibayar = $request->tahun_dibayar[$i];
            $input->id_spp = $request->id_spp;
            $input->jumlah_bayar = $request->jumlah_bayar[$i];
            // dd($input);
            $status=$input->save();
        }
        if($status) return redirect('/transaksi')->with('success','Data Berhasil Disimpan!');
        else return redirect('/transaksi')->with('error','Data gagal Disimpan!!');
    }
    public function histori(){
        $histori['histori'] = DB::table('pembayaran')->join('siswa','pembayaran.nisn','=','siswa.nisn')->join('kelas','siswa.id_kelas','=','kelas.id_kelas')->orderBy('id_pembayaran','desc')->get();

        if(Auth::user()->level == 'admin')
        {
            return view('histori')->with($histori);
        }
        elseif(Auth::user()->level == 'siswa')
        {
            return view('hs')->with($histori);
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SPP;

class SPPController extends Controller
{
    public function index()
    {
        $data['colec'] = \App\SPP::all();
        return view ('spp')->with($data);
    }
    public function create(){
        return view('spp/form');
    }
    public function store(Request $request){
        $rules=[
            'tahun' => 'required',
            'nominal' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = \App\SPP::create($input);

        if($status) return redirect('/spp')->with('success','Data Berhasil Disimpan!');
        else return redirect('/spp')->with('error','Data gagal Disimpan!!');
    }
    public function edit($id){
        $data = SPP::find($id);
        return view('spp/form',['spp'=>$data]);
    }
    public function update(Request $request){
        $rules=[
            'tahun' => 'required',
            'nominal' => 'required'
        ];
        $this->validate($request, $rules);

        $data = SPP::find($request->id_spp);
        $data->tahun = $request->tahun;
        $data->nominal = $request->nominal;
        $data->save();

        return redirect('/spp')->with('success','Data Berhasil Diubah!');
    }

    public function delete($id){
        $genre = SPP::find($id);
        $genre->delete();
        return redirect('/spp')->with('success','Data Berhasil DiHapus');
    }
}

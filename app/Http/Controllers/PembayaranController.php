<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembayaran;

class PembayaranController extends Controller
{
    public function index()
    {
        $data['pembayaran'] = \App\Pembayaran::all();
        return view ('pembayaran')->with($data);
    }
    public function create(){
        return view('pembayaran/form');
    }
    public function store(Request $request){
        $rules=[
            'nisn' => 'required',
            'id_petugas' => 'required',
            'tgl_bayar' => 'required',
            'bulan_dibayar' => 'required',
            'tahun_dibayar' => 'required',
            'jumlah_bayar' => 'required',
            'id_spp' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        // dd($input);
        $status = \App\Pembayaran::create($input);

        if($status) return redirect('/pembayaran')->with('success','Data Berhasil Disimpan!');
        else return redirect('/pembayaran')->with('error','Data gagal Disimpan!!');
    }
}

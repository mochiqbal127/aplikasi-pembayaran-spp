<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;

class SiswaController extends Controller
{
    public function index()
    {
        $data['siswa'] = \App\Siswa::all();
        return view ('siswa')->with($data);
    }
    public function pilihsiswa()
    {
        $data['siswa'] = \App\Siswa::all();
        return view ('pembayaran')->with($data);
    }
    public function create(){
        return view('siswa/form');
    }
    public function store(Request $request){
        $rules=[
            'nisn' => 'required',
            'nis' => 'required',
            'nama' => 'required',
            'id_kelas' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'id_spp' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        // dd($input);
        $status = \App\Siswa::create($input);

        if($status) return redirect('/siswa')->with('success','Data Berhasil Disimpan!');
        else return redirect('/siswa')->with('error','Data gagal Disimpan!!');
    }
    public function edit($id){
        $data = Siswa::find($id);
        return view('siswa/form',['siswa'=>$data]);
    }
    public function update(Request $request){
        $rules=[
            'nisn' => 'required',
            'nis' => 'required',
            'nama' => 'required',
            'id_kelas' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'id_spp' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $result = \App\Siswa::where('id_siswa',$request->id)->first();
        $status = $result->update($input);

        return redirect('/siswa')->with('success','Data Berhasil Diubah!');
    }

    public function delete($id){
        $siswa = Siswa::find($id);
        $siswa->delete();
        return redirect('/siswa')->with('success','Data Berhasil DiHapus');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \App\User::where('username', $request->username)->first();
        if ($user->level == 'admin') {
            return redirect('admin/dashboard');
        } elseif ($user->level == 'siswa') {
            return redirect('siswa/dashboard');
        }

        return $next($request);
    }
}

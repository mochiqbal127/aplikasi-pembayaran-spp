<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'IndexController@index');
    //Kelas CRUD
    Route::get('/kelas', 'KelasController@index')->name('kelas');
   // Route::get('/kelas/add' , 'KelasController@create');
    Route::post('/kelas' , 'KelasController@store');
   // Route::get('kelas/{id}/edit','KelasController@edit');
    Route::put('/kelas','KelasController@update');
    Route::get('kelas/{id}/hapus','KelasController@delete');
    Route::get('kelas/cetak','KelasController@cetak')->name('cetak-kelas');
    //SPP CRUD
    Route::get('/spp', 'SPPController@index')->name('spp');
    //Route::get('/spp/add' , 'SPPController@create');
    Route::post('/spp' , 'SPPController@store');
    //Route::get('spp/{id}/edit','SPPController@edit');
    Route::put('/spp','SPPController@update');
    Route::get('spp/{id}/hapus','SPPController@delete');
    //Petugas CRUD
    Route::get('/petugas', 'PetugasController@index')->name('petugas');
    //Route::get('/petugas/add' , 'PetugasController@create');
    Route::post('/petugas' , 'PetugasController@store');
    //Route::get('petugas/{id}/edit','PetugasController@edit');
    Route::put('/petugas','PetugasController@update');
    Route::get('petugas/{id}/hapus', 'PetugasController@delete');
    //Siswa CRUD
    Route::get('/siswa', 'SiswaController@index')->name('siswa');
    //Route::get('/siswa/add' , 'SiswaController@create');
    Route::post('/siswa' , 'SiswaController@store');
    //Route::get('siswa/{id}/edit','SiswaController@edit');
    Route::put('/siswa','SiswaController@update');
    Route::get('siswa/{id}/hapus', 'SiswaController@delete');

    //Pembayaran
    Route::get('/pembayaran', 'SiswaController@pilihsiswa')->name('pembayaran');
    Route::get('pembayaran/add' , 'PembayaranController@create');
    Route::post('pembayaran/add' , 'PembayaranController@store');
    Route::get('siswa/{id}/edit','SiswaController@edit');
    Route::put('siswa/{id}/edit','SiswaController@update');
    Route::get('siswa/{id}/hapus', 'SiswaController@delete');

    //Transaksi
    Route::get('/transaksi', 'TransactionController@index');
    Route::post('/transaksi', 'TransactionController@store');

    //Histori
    Route::get('/histori', 'TransactionController@histori');
    Route::get('/hs', 'TransactionController@histori');


});
